﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.IO;
using System.Windows.Forms;

namespace pageSuiteFTPmanager
{
    public class Class1
    {
        private static string password = "";
        private static string username = "";
        private static string host = "";

        private FtpWebRequest ftprequest = null;
        private FtpWebResponse ftpresponse = null;
        private Stream ftpStream = null;
        private int bufferSize = 2048;

        public void PagesuiteFTPConnect(string user, string pass, string hostname)
        {
            username = user;
            password = pass;
            host = hostname;
        }
        //public void DownloadFile(string remoteFile, string localFile)
        //{
        //    try
        //    {
        //        ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + remoteFile);
        //        ftprequest.Credentials = new NetworkCredential(username, password);
        //        ftprequest.UseBinary = true;
        //        ftprequest.UsePassive = true;
        //        ftprequest.KeepAlive = true;
        //        ftprequest.Method = WebRequestMethods.Ftp.DownloadFile;
        //        ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
        //        ftpStream = ftpresponse.GetResponseStream();
        //        FileStream fs = new FileStream(localFile, FileMode.OpenOrCreate);
        //        byte[] byteBuffer = new byte[Convert.ToInt32(getFileSize(remoteFile))];
        //        int bytesRead = ftpStream.Read(byteBuffer, 0, Convert.ToInt32(getFileSize(remoteFile)));

        //        try
        //        {
        //            while (bytesRead > 0)
        //            {
        //                fs.Write(byteBuffer, 0, bytesRead);
        //                bytesRead = ftpStream.Read(byteBuffer, 0, Convert.ToInt32(getFileSize(remoteFile)));
        //            }

        //        }

        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message, "Something went wrong");
        //        }

        //        fs.Close();
        //        ftpStream.Close();
        //        ftpresponse.Close();
        //        ftprequest = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Something went wrong");
        //    }
        //}

        //public void UploadFile(string localFile, string remoteFile)

        //{
        //    try
        //    {

        //        ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + remoteFile);
        //        ftprequest.Credentials = new NetworkCredential(username, password);
        //        ftprequest.UseBinary = true;
        //        ftprequest.UsePassive = true;
        //        ftprequest.KeepAlive = true;
        //        ftprequest.Method = WebRequestMethods.Ftp.UploadFile;
        //        FileStream lfs = new FileStream(localFile, FileMode.Open);
        //        byte[] bytebuffer = new byte[lfs.Length];
        //        int byteSend = lfs.Read(bytebuffer, 0, (int)lfs.Length);

        //        try
        //        {
        //            while (byteSend != -1)

        //            {
        //                byteSend = lfs.Read(bytebuffer, 0, (int)lfs.Length);
        //                ftpStream.Write(bytebuffer, 0, byteSend);
        //            }
        //        }

        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message, "Something went wrong");
        //        }
        //        ftpresponse.Close();
        //        ftpStream.Close();
        //        lfs.Close();
        //        ftprequest = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Something went wrong");
        //    }

        //}
        // i have a bug in the above code, fix this and we can download and upload files to our FTP servers :) checkout the Debug folder
        // for the form containing the additional features.

        public void Rename(string oldname, string newname)
        {
            try
            {
                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + oldname);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.Rename;
                ftprequest.RenameTo = newname;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpresponse.Close();
                ftprequest = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, talk to Baz");
            }
        }

        public string[] getFilesOnServer(string dir)
        {
            string[] filesInDir = new string[50];
            try
            {
                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + dir);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UsePassive = true;
                ftprequest.UseBinary = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.ListDirectory;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpStream = ftpresponse.GetResponseStream();
                StreamReader sr = new StreamReader(ftpStream);
                string dirRaw = null;
                try
                {
                    while (sr.Peek() != -1)
                    {
                        dirRaw += sr.ReadLine() + "|";
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Something went wrong, talk to Baz");
                }
                ftpresponse.Close();
                sr.Dispose();
                ftpStream.Close();
                ftprequest = null;
                try
                {
                    filesInDir = dirRaw.Split("|".ToCharArray());
                    return filesInDir;
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Something went wrong, talk to Baz");
                }
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, talk to Baz");
            }
            return filesInDir;
        }

        public void Delete(string filename)
        {

            try
            {

                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + filename);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.DeleteFile;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpresponse.Close();
                ftprequest = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, talk to Baz");
            }

        }

        public void Delete(string ftpDirectory)
        {

            try
            {

                ftprequest = (FtpWebRequest)FtpWebRequest.Create(ftpDirectory);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.RemoveDirectory;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpresponse.Close();
                ftprequest = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, talk to Baz");
            }

        }

        public void CreateDir(string name)
        {
            try
            {
                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + name);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpresponse.Close();
                ftprequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, talk to Baz");
            }
        }
        public long getFileSize(string filename)
        {
            long size;

            FtpWebRequest sizeRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + filename);
            sizeRequest.Credentials = new NetworkCredential(username, password);
            sizeRequest.Method = WebRequestMethods.Ftp.GetFileSize;
            sizeRequest.UseBinary = true;

            FtpWebResponse serverResponse = (FtpWebResponse)sizeRequest.GetResponse();
            FtpWebResponse respSize = (FtpWebResponse)sizeRequest.GetResponse();
            size = respSize.ContentLength;


            return size;

        }




        public bool IsRunning
        {
            get;
            private set;
        }
        public string FtpUserName
        {
            get;
            set;
        }
        public string FtpPassword
        {
            get;
            set;
        }
        public string FtpLocationToWatch
        {
            get;
            set;
        }
        public string DownloadTo
        {
            get;
            set;
        }
        public bool KeepOrignal
        {
            get;
            set;
        }
        public bool OverwriteExisting
        {
            get;
            set;
        }
        public int RecheckIntervalInSeconds
        {
            get;
            set;
        }
        private bool DownloadInprogress
        {
            get;
            set;
        }

        private System.Timers.Timer JobProcessor;
        //edit the details below to represent your own settings,
        public Class1(string FtpLocationToWatch = "ftp://yourFTPserverAddress/", string DownloadTo = @"logs.txt", int RecheckIntervalInSeconds = 1, string UserName = "yourFTPuserName", string Password = "yourFTPpass", bool KeepOrignal = false, bool OverwriteExisting = false)
        {
            this.FtpUserName = UserName;
            this.FtpPassword = Password;
            this.FtpLocationToWatch = FtpLocationToWatch;
            this.DownloadTo = DownloadTo;
            this.KeepOrignal = KeepOrignal;
            this.RecheckIntervalInSeconds = RecheckIntervalInSeconds;
            this.OverwriteExisting = OverwriteExisting;

            if (this.RecheckIntervalInSeconds < 1) this.RecheckIntervalInSeconds = 1;
        }

        public void StartDownloading()
        {

            JobProcessor = new Timer(this.RecheckIntervalInSeconds * 1000);
            JobProcessor.AutoReset = false;
            JobProcessor.Enabled = false;
            JobProcessor.Elapsed += (sender, e) =>
            {
                try
                {

                    this.IsRunning = true;

                    string[] FilesList = GetFilesList(this.FtpLocationToWatch, this.FtpUserName, this.FtpPassword);

                    if (FilesList == null || FilesList.Length < 1)
                    {
                        return;
                    }

                    foreach (string FileName in FilesList)
                    {
                        if (!string.IsNullOrWhiteSpace(FileName))
                        {
                            DownloadFile(this.FtpLocationToWatch, this.DownloadTo, FileName.Trim(), this.FtpUserName, this.FtpPassword, this.OverwriteExisting);

                            if (!this.KeepOrignal)
                            {
                                DeleteFile(Path.Combine(this.FtpLocationToWatch, FileName.Trim()), this.FtpUserName, this.FtpPassword);
                            }
                        }
                    }

                    this.IsRunning = false;
                    JobProcessor.Enabled = true;
                }

                catch (Exception exp)
                {
                    this.IsRunning = false;
                    JobProcessor.Enabled = true;
                    Console.WriteLine(exp.Message);
                }
            };

            JobProcessor.Start();
        }

        public void StopDownloading()
        {
            try
            {
                this.JobProcessor.Dispose();
                this.IsRunning = false;
            }
            catch { }
        }

        private void DeleteFile(string FtpFilePath, string UserName, string Password)
        {
            FtpWebRequest FtpRequest;
            FtpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(FtpFilePath));
            FtpRequest.UseBinary = true;
            FtpRequest.Method = WebRequestMethods.Ftp.DeleteFile;

            FtpRequest.Credentials = new NetworkCredential(UserName, Password);
            FtpWebResponse response = (FtpWebResponse)FtpRequest.GetResponse();
            response.Close();

        }
        private void DownloadFile(string FtpLocation, string FileSystemLocation, string FileName, string UserName, string Password, bool OverwriteExisting)
        {
            try
            {
                const int BufferSize = 2048;
                byte[] Buffer = new byte[BufferSize];

                FtpWebRequest Request;
                FtpWebResponse Response;

                if (File.Exists(Path.Combine(FileSystemLocation, FileName)))
                {
                    if (OverwriteExisting)
                    {
                        File.Delete(Path.Combine(FileSystemLocation, FileName));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("File {0} already exist.", FileName));
                        return;
                    }
                }

                Request = (FtpWebRequest)FtpWebRequest.Create(new Uri(Path.Combine(FtpLocation, FileName)));
                Request.Credentials = new NetworkCredential(UserName, Password);
                Request.Proxy = null;
                Request.Method = WebRequestMethods.Ftp.DownloadFile;
                Request.UseBinary = true;

                Response = (FtpWebResponse)Request.GetResponse();

                using (Stream s = Response.GetResponseStream())
                {
                    using (FileStream fs = new FileStream(Path.Combine(FileSystemLocation, FileName), FileMode.CreateNew, FileAccess.ReadWrite))
                    {
                        while (s.Read(Buffer, 0, BufferSize) != -1)
                        {
                            fs.Write(Buffer, 0, BufferSize);
                        }
                    }
                }
            }
            catch { }

        }
        private string[] GetFilesList(string FtpFolderPath, string UserName, string Password)
        {
            try
            {
                FtpWebRequest Request;
                FtpWebResponse Response;

                Request = (FtpWebRequest)FtpWebRequest.Create(new Uri(FtpFolderPath));
                Request.Credentials = new NetworkCredential(UserName, Password);
                Request.Proxy = null;
                Request.Method = WebRequestMethods.Ftp.ListDirectory;
                Request.UseBinary = true;

                Response = (FtpWebResponse)Request.GetResponse();
                StreamReader reader = new StreamReader(Response.GetResponseStream());
                string Data = reader.ReadToEnd();

                return Data.Split('\n');
            }
            catch
            {
                return null;
            }
        }
    }
}




