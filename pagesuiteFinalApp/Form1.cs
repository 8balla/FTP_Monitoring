﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using pageSuiteFTPmanager;
using ftpMonClass;


namespace pagesuiteFinalApp
{
    public partial class Form1 : Form
    {
        //private ftpMonClass watcher;
        pageSuiteFTPmanagerClass client;
        private System.IO.FileSystemWatcher m_watcher;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            client = new pageSuiteFTPmanagerClass(textBox1.Text, textBox2.Text, textBox3.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] files = client.getFilesOnServer("*");
            foreach (string filename in files)
            {
                DataGridView.Rows.Add(filename);
            }
            using (TextWriter writer = File.AppendText(@"logs.txt"))
            {
                Log("I cant get this working...", writer);

            }
            void Log(string logMessage, TextWriter w)
            {


                w.Write("\r\nStream time stamp: ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                w.Write("New/Changed files appear first:\n");
                w.WriteLine("\n");
               




                for (int i = 0; i < DataGridView.Rows.Count - 1; i++)
                {
                   
                    for (int j = 0; j < DataGridView.Columns.Count; j++)
                    {
                       
                        w.Write("" + DataGridView.Rows[i].Cells[j].Value.ToString() + "\t" + "\n");
                    }
                    w.WriteLine("");
                    w.WriteLine("--------------------------------------------------------------");

                }
               }

        }


        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            string ext = textBox4.Text.Substring(textBox4.Text.Length - 4, 4);
            sfd.Filter = "Download Files extension(" + ext + ")|*" + ext;
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox5.Text = sfd.FileName;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            client.DownloadFile(textBox4.Text, textBox5.Text);
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "All Files|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox6.Text = ofd.FileName;
                textBox7.Text = ofd.SafeFileName;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            client.UploadFile(textBox6.Text, textBox7.Text);
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            client.Rename(textBox8.Text, textBox9.Text);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            client.CreateDir(textBox10.Text);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            client.Delete(textBox11.Text);
        }

 




        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (Convert.ToString(DataGridView.Rows[e.RowIndex].Cells[0].Value).Contains("."))
            {
                return;
            }
            else
            {


                string[] Files = client.getFilesOnServer(DataGridView.Rows[e.RowIndex].Cells[0].Value.ToString());
                DataGridView.Rows.Clear();
                foreach (string File in Files) ;
                {
                    DataGridView.Rows.Add(Files);
                }
            }

        }
        public partial class datagridview_to_txt_file : Form1
        {
            public datagridview_to_txt_file()
            {
                InitializeComponent();
            }
            private void DataGridView1_to_txt_file_load(object sender, EventArgs e)
            {
                DataTable table = new DataTable();
                table.Columns.Add("FTP Files/Dir", typeof(string));

                DataGridView.DataSource = table;

            }
        }
        class ftpMonClass
        {
            public FileSystemEventHandler Deleted { get; internal set; }
            public bool IncludeSubdirectories { get; internal set; }
            public NotifyFilters NotifyFilter { get; internal set; }
            public FileSystemEventHandler Created { get; internal set; }
            public bool EnableRaisingEvents { get; internal set; }
            public FileSystemEventHandler Changed { get; internal set; }
            public string Filter { get; internal set; }

            public static implicit operator ftpMonClass(FileSystemWatcher v)
            {
                throw new NotImplementedException();
            }
        }


        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {

        }


        class Program
        {

        }


        void eventLog1_EntryWritten(object sender, System.Diagnostics.EntryWrittenEventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //watcher = new ftpMonClass();
            {
                watch();
            }

            void watch()
            {

                try
                {



                    Form1 obj = new Form1();
                    obj.Hide();
                    //watcher = new System.IO.FileSystemWatcher();
                    //watcher.Filter = "*.*";
                    //watcher.Deleted += new System.IO.FileSystemEventHandler(OnChanged);
                    //watcher.IncludeSubdirectories = true;
                    //watcher.NotifyFilter = System.IO.NotifyFilters.LastAccess | System.IO.NotifyFilters.LastWrite | System.IO.NotifyFilters.FileName | System.IO.NotifyFilters.DirectoryName | System.IO.NotifyFilters.CreationTime | System.IO.NotifyFilters.Size;
                    //watcher.Created += new System.IO.FileSystemEventHandler(OnChanged);
                    //watcher.EnableRaisingEvents = true;
                    //watcher.Changed += new System.IO.FileSystemEventHandler(OnChanged);
                    //watcher.Created += new System.IO.FileSystemEventHandler(OnChanged);
                    m_watcher = new System.IO.FileSystemWatcher();
                    m_watcher.Filter = "*.*";
                    m_watcher.Path = @"testsite";
                    m_watcher.IncludeSubdirectories = true;
                    m_watcher.NotifyFilter = System.IO.NotifyFilters.LastAccess | System.IO.NotifyFilters.LastWrite | System.IO.NotifyFilters.FileName | System.IO.NotifyFilters.DirectoryName | System.IO.NotifyFilters.CreationTime | System.IO.NotifyFilters.Size;
                    m_watcher.Created += new System.IO.FileSystemEventHandler(OnChanged);
                    m_watcher.EnableRaisingEvents = true;
                    m_watcher.Changed += new System.IO.FileSystemEventHandler(OnChanged);
                    m_watcher.Created += new System.IO.FileSystemEventHandler(OnChanged);
                    m_watcher.Deleted += new System.IO.FileSystemEventHandler(OnChanged);

                   



                    using (StreamWriter w = File.AppendText(@"logs.txt"))
                    {
                        Log("I cant get this working...", w);

                    }

                    using (StreamReader r = File.OpenText(@"logs.txt"))
                    {
                        DumpLog(r);
                    }

                  

                    void Log(string logMessage, TextWriter w)
                    {

                        w.Write("\r\nSystem started on: ");
                        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        w.WriteLine("Your FTP service has started: \n");
                        w.WriteLine("Monitoring has started on D testsite : \n");
                        w.WriteLine("New activity wil be displayed with a time stamp and message box: \n");
                        w.WriteLine("Files from your Server will display here: \n");
                        w.WriteLine("Changed folders or files will appear first: \n");
                        w.WriteLine("-------------------------------");
                        for (int i = 0; i < DataGridView.Rows.Count - 1; i++)
                        {

                            for (int j = 0; j < DataGridView.Columns.Count; j++)
                            {

                                w.Write("" + DataGridView.Rows[i].Cells[j].Value.ToString() + "\t" + "\n");
                            }
                            w.WriteLine("");
                            w.WriteLine("--------------------------------------------------------------");

                        }
                    }

                    void DumpLog(StreamReader r)
                    {
                        string line;
                        while ((line = r.ReadLine()) != null)
                        {
                            Console.WriteLine(line);
                        }
                    }

                }
                catch (Exception ex)
                {
                }
            }

            void OnChanged(object filter, System.IO.FileSystemEventArgs b)

            {
                using (StreamWriter w = File.AppendText(@"logs.txt"))
                {
                    Log("", w);

                }

                using (StreamReader r = File.OpenText(@"logs.txt"))
                {
                    DumpLog(r);
                }


                void Log(string logMessage, TextWriter w)
                {

                    w.WriteLine("\r\nThere has been a change to your folder testsite: \n ");
                    w.WriteLine("Details of the change are listed below: \n");
                    w.WriteLine(b.FullPath + "\n");
                    w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                        DateTime.Now.ToLongDateString());
                    w.WriteLine("-------------------------------");

                }


                void DumpLog(StreamReader r)
                {
                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }
                }

            }

            }
        

       

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
 
        private void button10_Click(object sender, EventArgs e)
        {
            DataGridView.Rows.Clear();
            DataGridView.Refresh();
        }

      
    }


    class FtpStyleUriParser {





    }
    class DirAppend
    {

    }

}
