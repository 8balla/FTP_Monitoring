﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.IO;
using System.Windows.Forms;

namespace pagesuiteFTPConnect
{
    public class Class1
    {
        private static string password = "";
        private static string username = "";
        private static string host = "";

        private FtpWebRequest ftprequest = null;
        private FtpWebResponse ftpresponse = null;
        private Stream ftpStream = null;
        private int bufferSize = 2048;

        public void PagesuiteFTPConnect(string user, string pass, string hostname)
        {
            username = user;
            password = pass;
            host = hostname;
        }
        public void DownloadFile(string remoteFile, string localFile)
        {
            try
            {
                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + remoteFile);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.DownloadFile;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpStream = ftpresponse.GetResponseStream();
                FileStream fs = new FileStream(localFile, FileMode.OpenOrCreate);
                byte[] byteBuffer = new byte[Convert.ToInt32(getFileSize(remoteFile))];
                int bytesRead = ftpStream.Read(byteBuffer, 0, Convert.ToInt32(getFileSize(remoteFile)));

                try
                {
                    while (bytesRead > 0)
                    {
                        fs.Write(byteBuffer, 0, bytesRead);
                        bytesRead = ftpStream.Read(byteBuffer, 0, Convert.ToInt32(getFileSize(remoteFile)));
                    }

                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
                }

                fs.Close();
                ftpStream.Close();
                ftpresponse.Close();
                ftprequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
            }
        }

            public void UploadFile(string localFile, string remoteFile)
  
            {
                try
                {

                    ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + remoteFile);
                    ftprequest.Credentials = new NetworkCredential(username, password);
                    ftprequest.UseBinary = true;
                    ftprequest.UsePassive = true;
                    ftprequest.KeepAlive = true;
                    ftprequest.Method = WebRequestMethods.Ftp.UploadFile;
                    FileStream lfs = new FileStream(localFile, FileMode.Open);
                    byte[] bytebuffer = new byte[lfs.Length];
                    int byteSend = lfs.Read(bytebuffer, 0, (int)lfs.Length);

                    try
                    {
                        while (byteSend != -1)

                        {
                            byteSend = lfs.Read(bytebuffer, 0, (int)lfs.Length);
                            ftpStream.Write(bytebuffer, 0, byteSend);
                        }
                    }

                    catch (Exception ex)
                    {
                    MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
                }
                    ftpresponse.Close();
                    ftpStream.Close();
                    lfs.Close();
                    ftprequest = null;
                }
                catch (Exception ex)
                {
                MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
            }

            }
        
            public void Rename(string oldname, string newname)
            {
            try
            {
                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + oldname);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.Rename;
                ftprequest.RenameTo = newname;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpresponse.Close();
                ftprequest = null;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
            }
        }

        public string[] getFilesOnServer(string dir)
        {
            string[] filesInDir = new string[50];
            try
            {
                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + dir);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UsePassive = true;
                ftprequest.UseBinary = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.ListDirectory;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpStream = ftpresponse.GetResponseStream();
                StreamReader sr = new StreamReader(ftpStream);
                string dirRaw = null;
                try
                {
                    while (sr.Peek() != -1)
                    {
                        dirRaw += sr.ReadLine() + "|";
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
                }
                ftpresponse.Close();
                sr.Dispose();
                ftpStream.Close();
                ftprequest = null;
                try
                {
                    filesInDir = dirRaw.Split("|".ToCharArray());
                    return filesInDir;
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
                }
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
            }
            return filesInDir;
        }

        public void Delete(string filename)
        {

            try
            {

                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + filename);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.DeleteFile;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpresponse.Close();
                ftprequest = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
            }

        }

        public void CreateDir(string name)
        {
            try
            {
                ftprequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + name);
                ftprequest.Credentials = new NetworkCredential(username, password);
                ftprequest.UseBinary = true;
                ftprequest.UsePassive = true;
                ftprequest.KeepAlive = true;
                ftprequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpresponse = (FtpWebResponse)ftprequest.GetResponse();
                ftpresponse.Close();
                ftprequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong, speak to Baz.");
            }
        }
        public long getFileSize(string filename)
        {
            long size;

            FtpWebRequest sizeRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + filename);
            sizeRequest.Credentials = new NetworkCredential(username, password);
            sizeRequest.Method = WebRequestMethods.Ftp.GetFileSize;
            sizeRequest.UseBinary = true;

            FtpWebResponse serverResponse = (FtpWebResponse)sizeRequest.GetResponse();
            FtpWebResponse respSize = (FtpWebResponse)sizeRequest.GetResponse();
            size = respSize.ContentLength;


            return size;

        }
    

        }
    }

